class Event:
    headers = ["ID", "KEYWORDS", "WHAT", "WHO", "WHERE", "WHEN"]

    def __init__(self, keywords, what, where, who):
        self.id = -2
        self.keywords = keywords
        self.where = where
        self.when = None
        self.who = who
        self.what = what
        self.day = 0
        self.tweets = set()
        self.strictness = 0
        self.ignored = False
        self.candidates = []
        self.is_confirmed = False
        self.is_merged = False

    """
    Events are equal if they share common keywords and entities 
    """
    def __eq__(self, other):
        if self.ignored or other.ignored:
            return False

        if self.id != -2 and other.id != -2:
            return self.id == other.id if self.id > -1 else False

        if not self.is_confirmed and not other.is_confirmed:

            if self.what.intersection(other.what) or (not self.what and not other.what):
                if self.who.intersection(other.who) or self.where.intersection(other.where):
                    print("WHO WHERE", str(self), str(other))
                    return True

                if self.who and other.who \
                        and (self.who.issubset(other.who)
                             or other.who.issubset(self.who)):
                    print("WHO", str(self), str(other))
                    return True

                if self.where and other.where \
                        and (self.where.issubset(other.where)
                             or other.where.issubset(self.where)):
                    print("WHERE", str(self), str(other))
                    return True

                if (self.what and not other.what) \
                        or (other.what and not self.what):
                    if self.who.intersection(other.who) \
                            or self.where.intersection(other.where):
                        print("WHAT", str(self), str(other))
                        return True

        else:
            if len(self.what.intersection(other.what)) > 0 * self.strictness \
                    or self.what.issubset(other.what) or other.what.issubset(self.what):
                    if len(self.who.intersection(other.who)) > 0 * self.strictness or len(self.where.intersection(other.where)) > 0 * self.strictness :
                        return True

        return False

    def enforce(self, other):
        if self.id != other.id:
            return

    """
    Merge duplicate events
    """
    def merge(self, other):
        #if self.ignored or other.ignored:
            #return
        self.keywords = self.keywords.union(other.keywords)
        self.where = self.where.union(other.where)
        self.who = self.who.union(other.who)
        self.what = self.what.union(other.what)
        self.tweets = self.tweets.union(other.tweets)
        self.is_merged = True
        self.candidates += other.candidates
        other.ignored = True

    """
    An event is valid if it has at least a keyword and a person or a place 
    """
    def is_valid(self):
        return not self.ignored or self.is_merged
        #and len(self.what.union(self.who).union(self.where)) > 1

    def to_array(self):
        return [self.id, ','.join(list(self.keywords)[:5]), ','.join(list(self.what)[:5]), ','.join(list(self.who)[:5]), ','.join(list(self.where)[:5]), self.candidates]

    def __str__(self):
        arr = [','.join(list(self.what)),','.join(list(self.who)),','.join(list(self.where)), self.when]
        return str(arr)