import csv
import helper
from Event import Event
import StreamManager
from helper import  TextHelper
from tabulate import tabulate
import utils
from optparse import OptionParser
from Score import *
from textrank import  *
collection = "fsd_dataset"
collectionDef = "all_tweets"
overall = []
col = {
    'fsd' : 'fsd_dataset',
    'event_2012' :'events_annotated',
    'event_purge' :'events_annotated_purge'
}

coldef = {
    'fsd' : 'fsd_dataset',
    'event_2012' :'all_tweets',
    'event_purge' :'all_tweets'
}



log = helper.enableLog()
helper.disableLog(log)

seen = []
texts = []
seen_terms = set()
visited = []
day = 0
initialGraph = None
toConfirm = []
tweets_ignored = []
considered = 0

whos,whats,wheres = set(), set(), set()
def build_graph(G, data):
    ignored = []
    global considered
    considered += len(data)
    for t in data:

        if not t['annotations']:
            ignored.append(t)
            continue

        ann = TextHelper.extract_entity_context(t)
        labels = [a['label'] for a in ann if 'label' in a]
        if not ann:
            ignored.append(t)
            continue
        for a in ann:
            if 'type' in a:
                if 'location' in a['type']:
                    wheres.add(a['label'].lower())
                elif 'person' in a['type'] or 'organization' in a['type'] or 'organisation' in a['type']:
                        whos.add(a['label'].lower())
                else:
                    whats.add(a['label'].lower())

            for l in a['edges']:
                if len(l[0].split()) < 3 and len(l[1].split()) < 3:
                    addEdge(G, l[0], l[1], t['id'], labels)
                else:  # nodes that have more than two tokens are most likely wrong NE identified by the NER tool(e.g. sandusky sentenced jail)
                    # thus, we split both initial nodes to create new edges
                    parts = ngrams(l[0].split() + l[1].split(), 2)
                    for p in parts:
                        addEdge(G, p[0], p[1], t['id'], labels)

    if ignored:
        parse_ignored(G,ignored)

def parse_ignored(G,ignored):
    global tweets_ignored
    ann = TextHelper.extract_word_context(ignored)
    for a in ann:
        if 'edges' in a and a['edges']:
            for edge in a['edges']:
                addEdge(G, edge[0], edge[1], a['id'], [])
        else:
            tweets_ignored.append(a['id'])


def extract_event_candidates(degree, graph, initialGraph, nodes):
    log.debug("Extracting events candidate...")
    res = []
    degree = [d[0] for d in degree]
    while degree:
        event_edges = []
        t = degree.pop(0)
        predecessors = [(p, t, graph.get_edge_data(p, t)['weight']) for p in graph.predecessors(t)]
        successors = [(t, s, graph.get_edge_data(t, s)['weight']) for s in graph.successors(t)]

        predecessors.sort(key=operator.itemgetter(2), reverse=True)  # extractKeyphrases() = itemgetter.ge
        successors.sort(key=operator.itemgetter(2), reverse=True)  # extractKeyphrases() = itemgetter.ge

        if predecessors:
            event_edges.append(predecessors[0])

        if successors:
            event_edges.append(successors[0])

        if not event_edges:
            continue

        toRem = set()
        for p in predecessors:
            for n in graph.neighbors(p[0]):
                if graph.neighbors(n) == 1:
                    degree = [d for d in degree if d==p[0]]
                    toRem.add((n, p[0]) if graph.has_edge(n,p[0]) else (p[0],n))
                    event_edges.append((n, p[0]) if graph.has_edge(n,p[0]) else (p[0],n))
        for p in successors:
            for n in graph.neighbors(p[1]):
                if graph.neighbors(n) == 1:
                    toRem.add((n, p[1]) if graph.has_edge(n, p[1]) else (p[1], n))
                    event_edges.append((n, p[1]) if graph.has_edge(n, p[1]) else (p[1], n))
                    degree = [d for d in degree if d == p[1]]

        keywords = set([l[0] for l in event_edges] + [l[1] for l in event_edges])

        tweets = set()
        for d in event_edges:
            dd = graph.get_edge_data(d[0], d[1])
            tweets = tweets.union(set(dd['id']))

        graph.remove_edges_from(toRem)

        if len(graph) > 0 and not nx.is_strongly_connected(graph):
            components = get_components(graph)
            for c in components:
                _nodes = c.nodes()
                if len(_nodes) <= 3:
                    for n in _nodes:
                        for t in toRem:
                            edge = None

                            if graph.has_edge(n, t[0]):
                                edge = (n,t[0])
                            elif graph.has_edge(n, t[1]):
                                edge = (n,t[1])
                            elif graph.has_edge(t[0], n):
                                edge = (t[0],n)
                            elif graph.has_edge(t[1],n):
                                edge = (t[1],n)
                            if edge:
                                tweets = tweets.union(graph.get_edge_data(edge[0], edge[1])['id'])

                    graph.remove_nodes_from(_nodes)
                    keywords = keywords.union(set(_nodes))
                    degree = [d for d in degree if d not in _nodes]

        e = Event(who=set([w for w in keywords if w in whos]),
                  where=set([w for w in keywords if w in wheres]),
                  what=set([w for w in keywords if w in whats]),
                  keywords=set([w for w in keywords if w not in whos.union(whats).union(wheres)]))
        e.tweets = tweets
        res.append(e)
    return res


def has_edge(graph, node1, node2):
    #origin, destination = None, None
    if graph.has_edge(node1, node2) :
        origin,destination = node1,node2
    elif graph.has_edge(node2, node1):
        origin, destination = node2, node1
    else:
        return False
    edges = graph.edges(nbunch=[origin], data='weight', default=1)
    edges.sort(key=operator.itemgetter(2), reverse = True)
    return edges[0][0] == destination or edges[0][1] == destination


def merge_duplicate_events(graph, res):
    global toConfirm
    log.debug("Merge duplicated events...")

    for elem in res:
        if not elem.is_valid():
            continue

        for i,t in enumerate(toConfirm):
            if not t.is_valid():
                continue
            if t == elem:
                elem.merge(t)
                toConfirm.pop(i)

            if not toConfirm:
                break

    toConfirm = [t for t in toConfirm if day - t.day < 2]

    #res = [elem for elem in res if 'ignore' not in elem and len(elem['tweets']) > 0 and len(elem['keys']) > 1*round]

    for i, elem in enumerate(res):

        for j in range(i + 1, len(res)):
            elem2 = res[j]
            if elem == elem2 :
                elem.merge(elem2)
                continue


    for elem in res:

        for s in seen:
            if s == elem:
                s.merge(elem)

    return [elem for elem in res if elem.is_valid()]


def process(opts):
    global initialGraph
    global  toConfirm
    collection = col[opts.dataset]
    collectionDef = coldef[opts.dataset]
    StreamManager.ne = opts.ne
    StreamManager.interval = opts.int
    StreamManager.init(opts.int,collection)
    tmin = opts.tmin
    min_weight = opts.wmin
    smin = opts.smin
    gts = StreamManager.gtEvents(limit=15)

    labels = [l[0] for l in gts]
    print(labels)

    initialGraph = nx.DiGraph()

    myfile=open('results_{}_{}.csv'.format(collection,smin), 'w')
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    wr.writerow(["GT", "#tweets", "Detected"])

    global seen
    seen = []
    global seen_terms
    while True:
        global  day
        data = StreamManager.tweets_event(1018)
        day = 12
        initialGraph.clear()
        log.debug("Building the graph")
        #build the graph from tweets
        build_graph(initialGraph, data)

        display(initialGraph)

        log.debug("Cleaning the graph")
        __nodes = degrees(initialGraph, nbunch=initialGraph.nodes())
        __nodes = [d if d[1] > 0 else (d[0], 1) for d in __nodes]

        _degree = getScore(initialGraph, __nodes, dangling=False)
        _degree = [d for d in _degree if d[1] >= smin]
        gg = clean(initialGraph, min_weight=min_weight)

        for graph in gg :
            log.debug("Retrieving nodes")
            nodes = graph.nodes(data=True)

            nodes= [n[0] for n in nodes]
            degree = [d for d in _degree if d[0] in nodes]

            #print(degree)

            log.debug("Ranking nodes")
            res = extract_event_candidates(degree, graph, initialGraph, nodes)

            log.debug("Pruning the graph")
            events = merge_duplicate_events(graph, res)
            if not events:
                log.debug("No event found...")
                continue
            news = []
            log.debug("Generating Description for {} candidates".format(len(events)))
            for event in events:
                event.day = day

                if len(event.tweets) < opts.mtweet :
                    toConfirm.append(event)
                    continue

                event.tweets = list(event.tweets)

                truths = AnnotationHelper.groundTruthEvent(collection,event.tweets)
                event.candidates = [(t['id'], len(t['tweets'])) for t in truths]

                tt = [e for e in truths if len(e['tweets']) * opts.percent]
                if len(tt) == 1:
                    event.id = tt[0]['id']

                else:
                    event.id = -1

                try:
                    seen.index(event)
                except:
                    seen.append(event)

                news.append(event.to_array())
                #seen_terms = seen_terms.union(r['keyss'])

            #overall.extend(news)
            if news:
                tt = tabulate(news, Event.headers)
                print(tt)
        break

    correct = set([s.id for s in seen if s.id > -1])
    incorrect = len([s.id for s in seen if s.id ==-1])
    print(correct, incorrect)
    precision = len(correct)/(incorrect+len(correct))
    recall = len(correct)/len(labels)
    f_score = 2*precision*recall/(precision+recall)

    print("Precision",precision)
    print("Recall",recall)
    print("F-Score",f_score)
    print(considered, len(tweets_ignored))
    print(tweets_ignored)

if __name__ == '__main__':
    parser = OptionParser('''%prog -o ontology -t type -f force ''')
    parser.add_option('-n', '--negative', dest='ne', default=0, type=int)
    parser.add_option('-t', '--tmin', dest='tmin', default=1, type=int)
    parser.add_option('-w', '--wmin', dest='wmin', default=0, type=int)
    parser.add_option('-i', '--int', dest='int', default=2, type=int)
    parser.add_option('-s', '--smin', dest='smin', default=0, type=float)
    parser.add_option('-m', '--mtweet', dest='mtweet', default=5, type=int)
    parser.add_option('-d', '--dataset', dest='dataset', default='fsd', type=str)
    parser.add_option('-p', '--percent', dest='percent', default=0.95, type=float)
    #print(res)
    opts, args = parser.parse_args()
    process(opts)