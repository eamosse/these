from helper import MongoHelper as db
from helper import TextHelper
import os
db.connect("tweets_dataset")
categories = ["Science","Attacks", "Politics","Miscellaneous","Arts","Sports","Accidents","Economy"]
all = ["Science","Attacks", "Politics","Miscellaneous","Arts","Sports","Accidents","Economy", "undefined"]
binaries = ["positive","negative"]
ontologies = ['dbpedia', 'yago']
types = ['generic', 'specific']


def parse(data, binary=False):
    texts = []
    for t in data:
        text = t['text']
        text = [str(t).strip() for t in text.split() if len(t) > 2]
        if len(text) > 1:
            #clazz = t['category'] if not binary else 'negative' if t['category']=='undefined' else 'positive'
            texts.append('{}\t{}\t{}'.format(t['tweet_id'],t['category'], ' '.join(text)))
    #texts.append('\n')
    return texts

def write(data, folder, file, binary=False):
    for ontology in ontologies:
        for type in types:
            _folder = "{}/{}/{}".format(folder,ontology,type)
            create(_folder)
            dada = []
            for d in data:
                print(d)
                for dd in d['annotations']:
                    if dd['ontology'] == ontology and dd['type'] == type:
                        dd['category'] = d['category']
                        dd['tweet_id'] = d['tweet_id']
                        dada.append(dd)
            with open("{}/{}".format(_folder,file), "w", encoding='utf-8') as f:
                texts = parse(dada, binary=binary)
                f.write('\n'.join(texts).strip())

def create(folder, destination="./"):
    if not os.path.exists("{}/{}".format(destination,folder)):
        os.makedirs("{}/{}".format(destination,folder))

def generate():

    data = db.find("annotated", query={"dataset":"event 2012", "category":{"$ne":"undefined"}})
    write(data=data,folder="train", file='positive.txt', binary=True)

    data = db.find("annotated", query={ "dataset":"event 2012", "category":"undefined"})
    write(data=data, folder="train", file='negative.txt', binary=True)

    data = db.find("annotated", query={"dataset":"fsd", "category":{"$ne":"undefined"}})
    write(data=data, folder="test", file='positive.txt', binary=True)

    data = db.find("annotated", query={"dataset":"fsd", "category":"undefined"})
    write(data=data, folder="test", file='negative.txt', binary=True)


def nbLines(file):
    num_lines = sum(1 for line in open(file))
    return num_lines

def buildModelForTrain(config):
    config['dataset'] = "fsd"
    sentences = db.find("annotated",query=config)
    write(sentences, folder="test/{}/{}".format(config['ontology'], config['type']),
          file="{}.txt".format(config['category']))

    config['dataset'] = "event 2012"
    sentences = db.find("annotated", query=config)
    write(sentences, folder="train/{}/{}".format(config['ontology'], config['type']),
          file="{}.txt".format(config['category']))

    #print(sentences)


def generateFileForIds(ids, ontology, type):
    file = "{}_{}.tsv".format(ontology, type)
    data = db.find("annotated", query={"tweet_id": {"$in":ids}, 'ontology':ontology, 'type':type})
    write(data=data,folder='eval',file=file)
    return 'eval/{}'.format(file)


def generateDataFile():
    pass
    """params = [
        {'ontology': 'dbpedia', 'type': 'generic', 'name': 'dbpedia_generic'},
        {'ontology': 'dbpedia', 'type': 'specific', 'name': 'dbpedia_specific'},
        {'ontology': 'yago', 'type': 'generic', 'name': 'yago_generic'},
        {'ontology': 'yago', 'type': 'specific', 'name': 'yago_specific'},
        {'ontology': 'dbpedia', 'type': 'normal', 'name': 'dbpedia_normal'},
    ]
    categories.append("undefined")
    for cat in categories:
        for config in params:
            config['category'] = cat
            if 'name' in config:
                del config['name']

            buildModelForTrain(config)
            generate(config['type'], config['ontology'])"""

def createTrainFile(classes, directory, name="neon_train"):
    data = []
    with open(name, "w", encoding='utf-8') as file:
        for index, cl in enumerate(classes):
            with open(os.path.join(directory, "{}.txt".format(cl))) as f:
                for line in f:
                    line = ' '.join(line.split('\t'))
                    line = TextHelper.tokenize(line)
                    if len(line) <=1:
                        continue
                    file.write("{}\t{}\n".format(index, ' '.join(line)))


if __name__ == '__main__':
    generate()