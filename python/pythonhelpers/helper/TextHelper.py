
from nltk.tokenize import TweetTokenizer
from nltk.corpus import stopwords
import nltk
import string
import helper
import numpy as np
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer
from sklearn.feature_extraction.text import TfidfVectorizer
import codecs
from helper.TweetPreprocessor import TweetPreprocessor
from helper import symspell
from operator import itemgetter
from nltk import ngrams
import re
import os
from helper.CMUTweetTagger import *



#log = helper.enableLog()
#helper.disableLog(log)
if not symspell.dictionary:
    #symspell.init()
    pass

data_dir = os.path.join(os.path.dirname(helper.__file__), 'data')
twitter_slang = os.path.join(data_dir, 'slang.txt')
twitter_stop = os.path.join(data_dir, 'stops.txt')
t = TweetPreprocessor()

wordnet_lemmatizer = WordNetLemmatizer()

tknzr = TweetTokenizer(strip_handles=True, reduce_len=True)
tp = TweetPreprocessor()
stop = stopwords.words('english') + list(string.punctuation)
default_stopwords = set(stop)
custom_stopwords = set(codecs.open(twitter_stop, 'r').read().splitlines())
twitter_slang = set(codecs.open(twitter_slang, 'r').read().splitlines())
all_stopwords = list(default_stopwords | custom_stopwords | twitter_slang)

porter = nltk.PorterStemmer()

def isStopWord(word):

    return word.lower() in all_stopwords

def tokenize(text, excerpt=[]):
    excerpt = [e.lower for e in excerpt]
    text = t.preprocess(text)
    #tokens = [token for token in tknzr.tokenize(text.lower()) if token in excerpt or token not in all_stopwords]
    tokens = [token for token in text.split() if (len(token) > 3 or token in excerpt ) and (token not in all_stopwords)]
    return tokens

def isInWordNet(word):
    word = wn.synsets(word)
    return word
"""
Compute the Leveinstein distance between the words
"""
def distance(word1, word2):
    return nltk.edit_distance(word1,word2)

def similarity(w1, w2, sim=wn.path_similarity):

  synsets1 = wn.synsets(w1)
  synsets2 = wn.synsets(w2)
  sim_scores = []
  for synset1 in synsets1:
    for synset2 in synsets2:
      sim_scores.append(sim(synset1, synset2))
  sim_scores = [sc for sc in sim_scores if sc]
  if len(sim_scores) == 0:
    return 0
  else:
    return max(sim_scores)

def slangs(text):
    return len(set(text.split()).intersection(custom_stopwords))

def lemmatize(word):
    word = word.lower()
    if not isInWordNet(word):
        word = symspell.best_word(word)
    wordv =  wordnet_lemmatizer.lemmatize(word, pos='v')
    if wordv == word:
        wordv = wordnet_lemmatizer.lemmatize(word, pos='n')
    return wordv

class MySentences(object):
    def __init__(self, files):
        self.files = files

    def __iter__(self):
        for fname in self.files:
            for line in open(fname):
                line = line.lower()
                _,__,text  = line.split('\t')
                tokens =  tokenize(text)
                yield [lemmatize(w) for w in tokens]

def tokenizer(text):
    text = text.lower()

    tokens = text.split(sep="=>")
    return [t for t in tokens if len(t) > 1]

def top_tfidf_feats(row, features, top_n=25):
    ''' Get top n tfidf values in row and return them with their corresponding feature names.'''
    topnids = np.argsort(row)[::-1]
    top_feats = {features[i]:row[i] for i in topnids}
    top_feats = {key:top_feats[key] for key in top_feats.keys() if top_feats[key] > 0}
    return top_feats

def top_feats_in_doc(Xtr, features, rowid, top_n=25):
    ''' Top tfidf features in specific document (matrix row) '''
    row = np.squeeze(Xtr[rowid].toarray())
    return top_tfidf_feats(row, features, top_n)

def buildTfIdf(docs):
    vectorizer = TfidfVectorizer(min_df=1, tokenizer=tokenizer)
    tfidf_matrix = vectorizer.fit_transform(docs)
    feature_names = vectorizer.get_feature_names()
    doc =  top_feats_in_doc(tfidf_matrix, feature_names, len(docs)-1)
    return  doc

def extract_entity_context(tweet, n=1):
    tweet = reIndex(tweet)
    text = tweet['text']
    mDicts = []
    if(tweet['annotations']):
        ents = []
        for ann in tweet['annotations']:
            if not ann['extractorType']:
                continue
            uris = str(ann['uri'] if ann['uri'] else ann['label']).split(sep="/")
            if len(ann['extractorType'].split(sep="/")) < 2:
                continue
            label = uris[len(uris)-1]
            if '(' in label:
                labels = label.split("_(")[:-1]
                label = '_('.join(labels)
            regex = re.compile('[,\.!?]')  # etc.
            label = regex.sub('', label).lower()
            if len(label) >= 3:
                text = text[0:ann['startChar']] + " " + label +" " + text[ann['endChar']+1:]
                mDict = {'label': label.lower(), 'type':ann['extractorType'].lower()}
                ents.append(label.lower())
                mDicts.append(mDict)

        """if stops(text) > 3 and len(ents) < 2:
            return []"""
        text = tokenize(text, excerpt=ents) #test
        text = [t if t in ents else symspell.get_suggestions(t, silent=True) for t in text]
        current = -1
        toRem = []
        for a in mDicts:
            current +=1
            try:
                index = text.index(a['label'])
                a['edges'] = []
                if index > 0 :
                    a['edges'].append((text[index-1], a['label'], 1))
                if index < len(text)-1:
                    a['edges'].append((a['label'], text[index + 1], 0))
            except Exception as e:
                #print(str(e))
                toRem.append(a)
                #return []

        for rem in toRem:
            mDicts.remove(rem)
            ents.remove(rem['label'])

        """gg = ngrams(ents,2)
        for g in gg:
            mDicts.append({'edges' :[(g[0], g[1], 2)]})"""

    else:
        print("==",tokenize(text))

    return mDicts

def extract_word_context(tweets):
    tt = [' '.join([tp if '#' not in tp else ' '.join(split_hash_tag(tp)) for tp in t['text'].split()]) for t in tweets]
    res = runtagger_parse(tt)
    conns = []
    for i,r in enumerate(res):
        origin = None
        destination = None
        last_type = None
        part = []
        for rr in r:
            if rr[1] in ['G', ',', 'E', 'O', '!', 'L', 'U', '@']:
                continue
            if last_type == rr[1] and last_type == '^':
                if destination:
                    destination += " " + rr[0]
                else:
                    origin += " " + rr[0]
            else:
                if origin and destination:
                    part.append((origin, destination,0))
                    origin = destination
                    destination = None

                if not origin:
                    origin = rr[0]
                else:
                    destination = rr[0]

            last_type = rr[1]

        if origin and destination:
            part.append((origin, destination,0))

        #print(part)
        conns.append({'id':tweets[i]['id'], 'edges':part})
        return conns

def reIndex(tweet, key='annotatins'):
    #print(tweet['id'])
    #start = tweet['end'] - len(tweet['text']) #tweet['start']
    tweet[key] = sorted(tweet[key], key=itemgetter('startChar'), reverse=True)

    """for ann in tweet[key]:
        try:
            ann['startChar'] = ann['startChar'] - start
            ann['endChar'] = ann['startChar'] + len(ann['label'])
            indexes = [m.start() for m in re.finditer(ann['label'].lower(), tweet['text'].lower())]
            current, small = -1, 0
            if not indexes:
                ann['relevance'] = 0
            for index in indexes:
                if ann['startChar'] - index < small or small == 0:
                    small = ann['startChar'] - index
                    current = index
            ann['startChar'] = current
            ann['endChar'] = ann['startChar'] + len(ann['label'])
        except:
            ann['relevance'] = 0
    tweet[key] = sorted(tweet[key], key=itemgetter('startChar'), reverse=True)
    tweet['start'] = 0"""

    return tweet

def NumberInt(nb):
    return nb

def ObjectId(val):
    return val

def ISODate(val):
    return val


def split_hash_tag(tag):
    return re.findall('[A-Z][^A-Z]*', tag)

def replacement(tweet):
    ontologies= ['extractorType','yagoType']
    types = ['generic','specific']
    results = []
    for ontology in ontologies:
        for _type in types:
            text = ""
            tweet['entities'] = sorted([ t for t in tweet['entities'] if 'extractorType' in t and t['extractorType'] !='#'], key=itemgetter('startChar'), reverse=False)
            last = 0
            for ent in tweet['entities']:
                if ent['startChar'] > last:
                    text += ' ' + tweet['tweet_text'][last:ent['startChar']]
                last = ent['endChar']

                if 'extractorType' in ent and ent['extractorType'] in ['TEMPORAL', 'U', 'Thing']:
                    if ent['extractorType'] == 'TEMPORAL':
                        text+= ' ' + 'TEMPORAL'
                    if ent['extractorType'] == 'Thing':
                        text += ent['label']
                    if ent['extractorType'] == 'U':
                        text+= ' '
                else:
                    if ontology in ent :
                        if not type(ent[ontology]) is list:
                            ent[ontology] = [c.strip() for c in ent[ontology][1:-1].split(',') if
                                             'Agent' not in c and 'Topic' not in c and 'Collection' not in c and 'Thing' not in c]
                        if ent[ontology]:
                            if _type == 'generic':
                                text += ' ' +ent[ontology][-1]
                            else:
                                text += ' ' +ent[ontology][0]
                        else:
                            text += ' ' +ent['label']
                    else:
                        text += ' '  + ent['label'] #tweet['tweet_text'][ent['startChar']:ent['endChar']]

            text += ' ' +tweet['tweet_text'][last:]
            text = ' '.join(text.split())
            results.append({'type':_type, 'ontology':'yago' if 'yago' in ontology else 'dbpedia', 'text':text})
    return results



if __name__ == '__main__':
    tweet = {
    "_id" : ObjectId("592173c981cb4b13829e9924"),
    "entities" : [
        {
            "extractorType" : "#",
            "startChar" : 39,
            "endChar" : 43,
            "label" : "#cnn"
        },
        {
            "extractorType" : "U",
            "startChar" : 43,
            "endChar" : 111,
            "label" : "http://www.cnn.com/2011/US/08/23/virginia.quake/index.html?hpt=hp_t1"
        },
        {
            "combinedType" : "[Earthquake, NaturalEvent, PhysicalEvent, Event]",
            "idEntity" : 29175801,
            "extractorType" : "[Earthquake, NaturalEvent, PhysicalEvent, Event]",
            "startChar" : 0,
            "confidence" : 0.6616,
            "yagoType" : "[Calamity, Misfortune, Trouble, Happening, Event]",
            "extractor" : "nerdml",
            "endChar" : 19,
            "label" : "Virginia earthquake",
            "nerdType" : "http://nerd.eurecom.fr/ontology#Thing",
            "uri" : "http://dbpedia.org/resource/2011_Virginia_earthquake",
            "relevance" : 0.5
        },
        {
            "combinedType" : "[AdministrativeDistrict, District, Region, Location]",
            "idEntity" : 29175802,
            "extractorType" : "[Thing]",
            "startChar" : 28,
            "confidence" : 0.649,
            "yagoType" : "[AdministrativeDistrict, District, Region, Location]",
            "extractor" : "nerdml",
            "endChar" : 38,
            "label" : "East Coast",
            "nerdType" : "http://nerd.eurecom.fr/ontology#Thing",
            "uri" : "http://dbpedia.org/resource/East_Coast_of_the_United_States",
            "relevance" : 0.5
        },
        {
            "combinedType" : "[TelevisionStation, Broadcaster, MediaOrganization, Organisation, Organization, Agent, Agent-Generic]",
            "idEntity" : 29175803,
            "extractorType" : "[TelevisionStation, Broadcaster, MediaOrganization, Organisation, Organization, Agent, Agent-Generic]",
            "startChar" : 39,
            "confidence" : 0.775908,
            "yagoType" : "[TalkShow, Broadcast, Show, SocialEvent, Event]",
            "extractor" : "nerdml",
            "endChar" : 42,
            "label" : "cnn",
            "nerdType" : "http://nerd.eurecom.fr/ontology#Organization",
            "uri" : "http://dbpedia.org/resource/CNN",
            "relevance" : 0.353868
        }
    ],
    "id" : "591dba47b760ee3b1b1543a9",
    "tweet_text" : "Virginia earthquake rattles East Coast #cnn http://www.cnn.com/2011/US/08/23/virginia.quake/index.html?hpt=hp_t1 …",
    "dataset" : "fsd",
    "tweet_type" : "event",
    "tweet" : ObjectId("56b6141e1d2def3c10c6d879"),
    "category" : "Disasters & Accidents",
    "tweet_id" : "106141124162367488"
}
    results = replacement(tweet)

    print(results)

